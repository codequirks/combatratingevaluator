import akka.actor.ActorSystem
import akka.http.scaladsl.Http.OutgoingConnection
import akka.http.scaladsl.model.{HttpHeader, HttpMethods, HttpRequest, HttpResponse, Uri}
import akka.http.scaladsl.{Http, HttpExt}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.duration._

object AkkaHttpsClient {
  def headers(headerTuples: (String, String)*): collection.immutable.Seq[HttpHeader] = {
    collection.immutable.Seq(headerTuples.toSeq.map(t => HttpHeader.parse(t._1, t._2)).flatMap {
      case ok: HttpHeader.ParsingResult.Ok => Some(ok.header)
      case _                               => None
    }: _*)
  }
}

class AkkaHttpsClient(host: String, port: Int)(headers: collection.immutable.Seq[HttpHeader] = List()) {
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executor: ExecutionContextExecutor = actorSystem.dispatcher
  implicit val http: HttpExt = Http()
  val timeout: FiniteDuration = 1.minute

  implicit lazy val httpConnection: Flow[HttpRequest, HttpResponse, Future[OutgoingConnection]] =
    http.outgoingConnectionHttps(host, port)

  def getStr(path: String, params: (String, String)*)(httpHeaders: collection.immutable.Seq[HttpHeader] = headers): String =
    Await.result(doRaw(get(path, params: _*)(httpHeaders)).flatMap(_.entity.toStrict(timeout).map(_.data.utf8String)), timeout)

  def get(path: String, params: (String, String)*)(httpHeaders: collection.immutable.Seq[HttpHeader] = headers): HttpRequest = HttpRequest(
    uri = Uri(path).withQuery(Uri.Query(params: _*)),
    headers = httpHeaders,
    method = HttpMethods.GET)

  def doRaw(request: HttpRequest): Future[HttpResponse] =
    Source
      .single(request)
      .via(httpConnection)
      .runWith(Sink.head)

  def terminate(): Unit = {
    actorSystem.terminate()
  }
}
