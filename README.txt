This project is written in Scala using SBT as a build tool.

The first thing you need to do is open src/main/scala/CombatRatingEvaluator.scala and insert your Bungie API Key
into the appropriate variable.

Once you've done that, if you have SBT installed all you need to do is execute the following via the command-line:
> sbt clean run

If you do not have SBT installed, visit the following URL and follow the provided instructions:
https://www.scala-sbt.org/1.x/docs/Setup.html

Please be aware that this is a test script, and as such the error reporting is minimal. For example, an invalid API
key results in a very vague "java.util.NoSuchElementException: key not found: Response".
