name := "SauronELO"

version := "1.0.0-SNAPSHOT"

organization := "Crias"

resolvers += "YoppWorks Repository" at "https://nexus.yoppworks.com:8443/repository/maven-releases/"

libraryDependencies += "com.typesafe.akka"          %% "akka-http"                % "10.1.12"
libraryDependencies += "com.typesafe.akka"          %% "akka-stream"              % "2.5.31"
libraryDependencies += "com.lihaoyi" %% "upickle" % "0.7.1"


