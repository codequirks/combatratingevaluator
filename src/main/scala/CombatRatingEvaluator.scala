import ujson.Value

import scala.util.{Failure, Success, Try}

object CombatRatingEvaluator extends App {
  val bungieApiKey = "REPLACE-ME"
  val playerName = "GrizzlyBigfoot9".replaceAll(" ", "%20")
  val http = new AkkaHttpsClient("www.bungie.net", 443)(AkkaHttpsClient.headers("X-API-Key" -> bungieApiKey))

  /*
    Try is a Scala construct that captures all errors. If I don't terminate the HTTP client the executable will run
    forever, so the Try lets me just naively execute code and still trust the http.terminate() will execute.
    It's not _quite_ used like this in production code (usually) but it does the trick for a test script.

    My player search is also incredibly naive for test purposes, since DestinyKD already searches players cleanly.
    However, once I've got the playerId the rest is pretty much

    The one piece I needed to do "safely" is the stat lookups (see safeInt/safeDouble) since it's very possible any
    player I test against might not have played all 9 modes. The "safe" functions just give me 0 defaults if the JSON
    path breaks.
   */
  Try {
    val playerJson = ujson.read(http.getStr(s"/platform/Destiny2/SearchDestinyPlayer/1/$playerName/")())
    val player = playerJson("Response")(0)
    println(s"FOUND ${player("displayName").str}")

    val playerId = player("membershipId").str
    val membershipType = player("membershipType").num.toInt
    val charactersJson = ujson.read(http.getStr(s"/platform/Destiny2/$membershipType/Profile/$playerId/", "components" -> "200")())
    val characters = charactersJson("Response")("characters")("data").obj

    print(s"OVERALL ")
    processCharacter(membershipType, playerId, "0")
    characters.foreach { case (characterId, characterData) =>
      val characterClass = characterData("classType").num.toInt match {
        case 0 => "TITAN"
        case 1 => "HUNTER"
        case 2 => "WARLOCK"
      }
      print(s"$characterClass ")
      processCharacter(membershipType, playerId, characterId)
    }
  } match {
    case Success(_) => println("\nScript executed successfully.")
    case Failure(ex) => ex.printStackTrace()
  }

  def processCharacter(membershipType: Int, playerId: String, characterId: String): Unit = {
    val response = http.getStr(s"/platform/Destiny2/$membershipType/Account/$playerId/Character/$characterId/Stats/", "modes" -> "43,73,48,37,80,84,25,71,59")()
    val jsonData = ujson.read(response)

    val ironBannerGames = safeInt(jsonData("Response")("ironBannerControl")("allTime")("activitiesEntered")("basic")("value"))
    val ironBannerCr = safeDouble(jsonData("Response")("ironBannerControl")("allTime")("combatRating")("basic")("value"))

    val controlGames = safeInt(jsonData("Response")("controlQuickplay")("allTime")("activitiesEntered")("basic")("value"))
    val controlCr = safeDouble(jsonData("Response")("controlQuickplay")("allTime")("combatRating")("basic")("value"))

    val rumbleGames = safeInt(jsonData("Response")("rumble")("allTime")("activitiesEntered")("basic")("value"))
    val rumbleCr = safeDouble(jsonData("Response")("rumble")("allTime")("combatRating")("basic")("value"))

    val survivalGames = safeInt(jsonData("Response")("survival")("allTime")("activitiesEntered")("basic")("value"))
    val survivalCr = safeDouble(jsonData("Response")("survival")("allTime")("combatRating")("basic")("value"))

    val eliminationGames = safeInt(jsonData("Response")("elimination")("allTime")("activitiesEntered")("basic")("value"))
    val eliminationCr = safeDouble(jsonData("Response")("elimination")("allTime")("combatRating")("basic")("value"))

    val trialsGames = safeInt(jsonData("Response")("trials_of_osiris")("allTime")("activitiesEntered")("basic")("value"))
    val trialsCr = safeDouble(jsonData("Response")("trials_of_osiris")("allTime")("combatRating")("basic")("value"))

    val mayhemGames = safeInt(jsonData("Response")("allMayhem")("allTime")("activitiesEntered")("basic")("value"))
    val mayhemCr = safeDouble(jsonData("Response")("allMayhem")("allTime")("combatRating")("basic")("value"))

    val clashGames = safeInt(jsonData("Response")("clashQuickplay")("allTime")("activitiesEntered")("basic")("value"))
    val clashCr = safeDouble(jsonData("Response")("clashQuickplay")("allTime")("combatRating")("basic")("value"))

    //val showdownGames = safeInt(jsonData("Response")("showdown")("allTime")("activitiesEntered")("basic")("value"))
    //val showdownCr =s safeDouble(jsonData("Response")("showdown")("allTime")("combatRating")("basic")("value"))

    val casualGames = ironBannerGames + controlGames + rumbleGames
    val casualCr =
      (ironBannerCr * (ironBannerGames.toDouble / casualGames)) +
        (controlCr * (controlGames.toDouble / casualGames)) +
        (rumbleCr * (rumbleGames.toDouble / casualGames))

    val competitiveGames = survivalGames + trialsGames + eliminationGames
    val competitiveCr =
      (survivalCr * (survivalGames.toDouble / competitiveGames)) +
        (trialsCr * (trialsGames.toDouble / competitiveGames)) +
        (eliminationCr * (eliminationGames.toDouble / competitiveGames))

    // NOTE: I want to include Showdown, but CRs for that are abnormal - between 5x and 10x higher than other CRs
    val rotatorGames = clashGames + mayhemGames
    val rotatorCr =
      (clashCr * (clashGames.toDouble / rotatorGames)) +
        (mayhemCr * (mayhemGames.toDouble / rotatorGames))

    val overallGames = casualGames + competitiveGames + rotatorGames
    val overallCr =
      (casualCr * (casualGames.toDouble / overallGames)) +
        (competitiveCr * (competitiveGames.toDouble / overallGames)) +
        (rotatorCr * (rotatorGames.toDouble / overallGames))

    println(
      s"""Character Combat Ratings:
         |    Casual:       $casualCr ($casualGames games played)
         |    Competitive:  $competitiveCr ($competitiveGames games played)
         |    Rotator:      $rotatorCr ($rotatorGames games played)
         |
         |    Overall:      $overallCr ($overallGames games played)
         |""".stripMargin)
  }

  http.terminate()

  def safeInt(i: => Value): Int = Try(i.num.toInt).getOrElse(0)
  def safeDouble(i: => Value): Double = Try(i.num).getOrElse(0.0)
}

